import React, { Component } from "react";
import RawEvents from "../components/Events";

const EVENTS_URL = "https://www.jasonbase.com/things/ePBa.json";

export default class Events extends Component {
  state = {
    loading: true,
    events: []
  };

  componentDidMount() {
    fetch(EVENTS_URL).then(res => res.json()).then(resJson => {
      this.setState({
        loading: false,
        events: resJson.data
      });
    });
  }

  render() {
    return <RawEvents {...this.state} />;
  }
}
