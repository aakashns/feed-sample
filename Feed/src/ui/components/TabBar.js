import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity as Touchable
} from "react-native";

const styles = StyleSheet.create({
  container: {
    height: 72,
    backgroundColor: "rgb(119,84,182)",
    paddingLeft: 28,
    paddingRight: 28,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center"
  },
  icon: {
    height: 32,
    width: 32,
    marginBottom: 4
  },
  label: {
    color: "#fff",
    fontSize: 12
  }
});

const Item = ({ label, active, source }) => (
  <Touchable onPress={() => console.log("Tab selected", label)}>
    <View style={{ alignItems: "center", opacity: active ? 1 : 0.6 }}>
      <Image style={styles.icon} source={source} />
      <Text style={styles.label}>
        {label}
      </Text>
    </View>
  </Touchable>
);

const TabBar = () => (
  <View style={styles.container}>
    <Item label="Feed" active source={require("../../img/ic_home.png")} />
    <Item label="Search" source={require("../../img/ic_search.png")} />
    <Item label="Werwalter" source={require("../../img/ic_key.png")} />
  </View>
);

export default TabBar;
