import React from "react";
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  ActivityIndicator
} from "react-native";
import Event from "./Event";

const styles = StyleSheet.create({
  container: {
    marginLeft: 14,
    marginRight: 14,
    flex: 1,
    shadowColor: "#000",
    shadowRadius: 1,
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowOpacity: 0.4,
    marginTop: -36,
    backgroundColor: "rgba(0,0,0,0)"
  },
  list: {
    borderTopLeftRadius: 2,
    borderTopRightRadius: 2,
    backgroundColor: "#fff",
    paddingLeft: 14,
    paddingRight: 14
  }
});

const Separator = () => (
  <View
    style={{
      backgroundColor: "rgb(236, 239, 242)",
      height: 1
    }}
  />
);

const Events = ({ loading, events }) => (
  <View style={styles.container}>
    {loading
      ? <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator animating />
        </View>
      : <FlatList
          style={styles.list}
          data={events}
          renderItem={({ item }) => <Event {...item} />}
          ItemSeparatorComponent={Separator}
          keyExtractor={x => x.id}
        />}
  </View>
);

export default Events;
