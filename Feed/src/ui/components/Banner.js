import React from "react";
import { View, Text, StyleSheet, Image } from "react-native";
import { BlurView } from "react-native-blur";

const styles = StyleSheet.create({
  container: {},
  image: {
    width: "100%",
    height: 124,
    opacity: 0.8
  },
  infoWrapper: {
    backgroundColor: "rgba(0,0,0,0)",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    marginBottom: 36
  },
  logo: {
    height: 72,
    width: 72,
    marginRight: 14
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
    maxWidth: 120,
    color: "#fff",
    textShadowColor: "#000",
    textShadowRadius: 2,
    textShadowOffset: {
      width: 1,
      height: 1
    }
  }
});

const blur = (
  <BlurView
    blurType="light"
    blurAmount={1}
    style={{ width: "100%", height: "100%" }}
  />
);

const Banner = ({ title }) => (
  <View style={styles.container}>
    <Image
      source={require("../../img/adventure.jpg")}
      resizeMode="cover"
      style={styles.image}
    >
      <View style={styles.infoWrapper}>
        <Image
          style={styles.logo}
          source={require("../../img/club_logo.png")}
        />
        <Text style={styles.title}>Adventure Club</Text>
      </View>
    </Image>
  </View>
);

export default Banner;
