import React from "react";
import { View, Text, Image, StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    paddingTop: 14,
    paddingBottom: 14,
    flexDirection: "row"
  },
  icon: {
    height: 28,
    width: 28
  },
  title: {
    fontSize: 17,
    color: "#593596",
    fontWeight: "bold"
  },
  location: {
    fontSize: 12,
    color: "#333",
    marginTop: 4,
    marginBottom: 8
  },
  timing: {
    fontSize: 12,
    color: "rgb(165,165,165)"
  }
});

const Event = ({ title, location, date, time, postTime, type }) => (
  <View style={styles.container}>
    <View
      style={{
        borderRadius: 22,
        backgroundColor: type === "event"
          ? "rgb(174,174,174)"
          : "rgb(247,179,86)",
        height: 44,
        width: 44,
        marginRight: 14,
        padding: 8
      }}
    >
      <Image
        style={styles.icon}
        resizeMode="contain"
        source={
          type === "event"
            ? require("../../img/ic_event_white_48pt.png")
            : require("../../img/ic_chat_bubble_white_48pt.png")
        }
      />
    </View>
    <View style={{ flex: 1 }}>
      <Text style={styles.title}>
        {title}
      </Text>
      <Text style={styles.location}>
        {location}
      </Text>
      {time &&
        <Text style={[styles.timing, { marginBottom: 4 }]}>
          {time}
        </Text>}
      <Text style={styles.timing}>
        {date}
      </Text>
    </View>
    <View style={{ paddingTop: 2 }}>
      <Text style={styles.timing}>{postTime}</Text>
    </View>
  </View>
);

export default Event;
