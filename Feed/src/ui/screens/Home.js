import React, { Component } from "react";
import { View, Text, StyleSheet, StatusBar } from "react-native";
import Banner from "../components/Banner";
import Events from "../containers/Events";
import TabBar from "../components/TabBar";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgb(234, 237, 242)"
  },
  statusBar: {
    height: 20,
    backgroundColor: "rgb(51, 51, 51)"
  }
});

export default class Home extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.statusBar}>
          <StatusBar barStyle="light-content" />
        </View>
        <Banner />
        <Events />
        <TabBar />
      </View>
    );
  }
}
